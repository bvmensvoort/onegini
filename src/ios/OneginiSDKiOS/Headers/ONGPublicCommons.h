//  Copyright (c) 2016 Onegini. All rights reserved.

#import <Foundation/Foundation.h>
#import "ONGPublicDefines.h"

NS_ASSUME_NONNULL_BEGIN

ONG_EXTERN NSString *const ONGSDKInitializationException;

/**
 The name of the notification send to listeners to inform them that the embedded web view has been closed.
 This is only used in if the ONGConfigModel kOGUseEmbeddedWebview is set to true.
 */
ONG_EXTERN NSString *const ONGCloseWebViewNotification DEPRECATED_MSG_ATTRIBUTE("Onegini SDK no longer distinguishes how "
    "registration request URL is handled. If registration request URL was handled by within UIWebView, it could be closed "
    "as soon as you send response to ONGRegistrationRequestChallengeSender or when you receive userClient:didReceivePinRegistrationChallenge: "
    "on your ONGRegistrationDelegate.");

NS_ASSUME_NONNULL_END