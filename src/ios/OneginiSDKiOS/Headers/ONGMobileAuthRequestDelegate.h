//  Copyright (c) 2016 Onegini. All rights reserved.

#import <Foundation/Foundation.h>

@class ONGUserClient;
@class ONGPinChallenge;
@class ONGFingerprintChallenge;
@class ONGFIDOChallenge;
@class ONGMobileAuthRequest;
@class ONGUserProfile;

NS_ASSUME_NONNULL_BEGIN

/**
 *  Protocol describing interface for objects implementing methods required to complete mobile authentication request.
 */
@protocol ONGMobileAuthRequestDelegate<NSObject>

/**
 *  Method called when mobile authentication request requires only confirmation to be completed.
 *
 *  @param userClient user client that received mobile authentication request
 *  @param confirmation confirmation block that needs to be invoked with confirmation value
 *  @param request mobile authentication request received by the SDK
 *
 *  @see ONGMobileAuthRequest
 */
- (void)userClient:(ONGUserClient *)userClient didReceiveConfirmationChallenge:(void (^)(BOOL confirmRequest))confirmation forRequest:(ONGMobileAuthRequest *)request;

@optional

/**
 *  Method called when mobile authentication request requires PIN code for confirmation.
 *
 *  @param userClient user client performing authentication
 *  @param challenge pin challenge used to complete authentication
 *  @param request mobile authentication request received by the SDK
 *
 *  @see ONGMobileAuthRequest
 */
- (void)userClient:(ONGUserClient *)userClient didReceivePinChallenge:(ONGPinChallenge *)challenge forRequest:(ONGMobileAuthRequest *)request;

/**
 *  Method called when authentication action requires FIDO authentication to continue. Its called before asking user for
 *  FIDO authentication.
 *  If its not implemented SDK will fallback to PIN code confirmation.
 *
 *  @param userClient user client performing authentication
 *  @param challenge FIDO challenge used to complete authentication
 *  @param request mobile authentication request received by the SDK
 *
 *  @see ONGMobileAuthRequest, ONGFIDOChallenge
 */
- (void)userClient:(ONGUserClient *)userClient didReceiveFIDOChallenge:(ONGFIDOChallenge *)challenge forRequest:(ONGMobileAuthRequest *)request;

/**
 *  Method called when authentication action requires TouchID to continue. Its called before asking user for fingerprint.
 *  If its not implemented SDK will fallback to PIN code confirmation.
 *
 *  @param userClient user client performing authentication
 *  @param challenge fingerprint challenge used to complete authentication
 *  @param request mobile authentication request received by the SDK
 *
 *  @see ONGMobileAuthRequest, ONGFingerprintChallenge
 */
- (void)userClient:(ONGUserClient *)userClient didReceiveFingerprintChallenge:(ONGFingerprintChallenge *)challenge forRequest:(ONGMobileAuthRequest *)request;

/**
 *  Method called when mobile authentication request handling did fail.
 *
 *  The returned error will be either within the ONGGenericErrorDomain, ONGMobileAuthRequestErrorDomain or ONGAuthenticationErrorDomain.
 *
 *  @param userClient user client performing authentication
 *  @param request mobile authentication request received by the SDK
 *  @param error error describing failure reason
 *
 *  @see ONGMobileAuthRequest
 */
- (void)userClient:(ONGUserClient *)userClient didFailToHandleMobileAuthRequest:(ONGMobileAuthRequest *)request error:(NSError *)error;

/**
 *  Method called when mobile authentication request handled successfully.
 *
 *  @param userClient user client performing authentication
 *  @param request mobile authentication request received by the SDK
 *
 *  @see ONGMobileAuthRequest
 */
- (void)userClient:(ONGUserClient *)userClient didHandleMobileAuthRequest:(ONGMobileAuthRequest *)request;

@end

NS_ASSUME_NONNULL_END
