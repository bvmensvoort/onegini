//  Copyright © 2016 Onegini. All rights reserved.

#import <Foundation/Foundation.h>

@class ONGUserClient;
@class ONGAuthenticator;
@class ONGUserProfile;
@class ONGPinChallenge;
@class ONGFingerprintChallenge;

NS_ASSUME_NONNULL_BEGIN

/**
 *  Protocol describing interface for objects implementing methods required to register authenticator.
 *  All invocations are performed on the main queue.
 */
@protocol ONGAuthenticatorRegistrationDelegate <NSObject>

@optional

/**
 *  Method called when authenticator registration is started.
 *
 *  @param userClient user client performing authenticator registration
 *  @param authenticator authenticator being registered
 *  @param userProfile user profile for which authenticator registration is started
 */
- (void)userClient:(ONGUserClient *)userClient didStartRegisteringAuthenticator:(ONGAuthenticator *)authenticator forUser:(ONGUserProfile *)userProfile;

/**
 *  Method called when authenticator registration is completed with success.
 *
 *  @param userClient user client performing authenticator registration
 *  @param authenticator registered authenticator
 *  @param userProfile user profile for which authenticator was registered
 */
- (void)userClient:(ONGUserClient *)userClient didRegisterAuthenticator:(ONGAuthenticator *)authenticator forUser:(ONGUserProfile *)userProfile;

/**
 *  Method called when authenticator registration failed with an error.
 *
 *  The returned error will be either within the ONGGenericErrorDomain, ONGAuthenticatorRegistrationErrorDomain or ONGAuthenticationErrorDomain.
 *
 *  @param userClient user client performing authenticator registration
 *  @param authenticator authenticator whose registration failed
 *  @param userProfile user profile for which authenticator registration failed
 *  @param error error describing cause of an error
 */
- (void)userClient:(ONGUserClient *)userClient didFailToRegisterAuthenticator:(ONGAuthenticator *)authenticator forUser:(ONGUserProfile *)userProfile error:(NSError *)error;

/**
 *  Method called when authenticator registration requires PIN code to continue.
 *
 *  @param userClient user client performing authenticator registration
 *  @param challenge pin challenge used to complete authentication
 */
- (void)userClient:(ONGUserClient *)userClient didReceivePinChallenge:(ONGPinChallenge *)challenge;

@end

NS_ASSUME_NONNULL_END

