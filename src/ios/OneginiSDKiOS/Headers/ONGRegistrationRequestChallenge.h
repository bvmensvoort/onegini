// Copyright (c) 2017 Onegini. All rights reserved.

#import <Foundation/Foundation.h>

@class ONGUserProfile;
@class ONGRegistrationRequestChallenge;

#pragma clang diagnostic push
#pragma ide diagnostic ignored "OCUnusedMethodInspection"
#pragma ide diagnostic ignored "OCUnusedPropertyInspection"

NS_ASSUME_NONNULL_BEGIN

/**
 * Protocol describing SDK object waiting for response to registration request challenge.
 */
@protocol ONGRegistrationRequestChallengeSender<NSObject>

/**
 * Method providing the URL to the SDK.
 *
 * @param url URL provided by the user
 * @param challenge registration request challenge for which the response is made
 */
- (void)respondWithURL:(NSURL *)url challenge:(ONGRegistrationRequestChallenge *)challenge;

/**
 * Method to cancel challenge
 *
 * @param challenge registration request challenge that needs to be cancelled
 */
- (void)cancelChallenge:(ONGRegistrationRequestChallenge *)challenge;

@end

/**
 * Represents registration request challenge. It provides all information about the challenge and a sender awaiting for a response.
 */
@interface ONGRegistrationRequestChallenge : NSObject

/**
 * User profile for which registration request challenge was sent.
 */
@property (nonatomic, readonly) ONGUserProfile *userProfile;

/**
 * URL used to perform a registration code request.
 */
@property (nonatomic, readonly) NSURL *url;

/**
 * Error describing cause of failure of previous challenge response.
 * Possible error domains: ONGGenericErrorDomain
 */
@property (nonatomic, readonly, nullable) NSError *error;

/**
 * Sender awaiting for response to the registration request challenge.
 */
@property (nonatomic, readonly) id<ONGRegistrationRequestChallengeSender> sender;

@end

NS_ASSUME_NONNULL_END

#pragma clang diagnostic pop
