// Copyright (c) 2016 Onegini. All rights reserved.

#import <Foundation/Foundation.h>

#ifndef ONG_PUBLIC_DEFINES
#define ONG_PUBLIC_DEFINES

#define ONG_EXTERN FOUNDATION_EXTERN

#define ONG_UNAVAILABLE NS_UNAVAILABLE

#endif
