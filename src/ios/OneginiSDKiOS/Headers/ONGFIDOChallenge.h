// Copyright (c) 2016 Onegini All rights reserved.

#import <Foundation/Foundation.h>

@class ONGFIDOChallenge;
@class ONGUserProfile;
@class ONGAuthenticator;

NS_ASSUME_NONNULL_BEGIN

/**
 * Protocol describing the SDK object waiting for response to authenticate with a FIDO challenge.
 */
@protocol ONGFIDOChallengeSender<NSObject>

/**
 * The method providing the confirmation response with default prompt to the SDK.
 *
 * @param challenge FIDO challenge for which the response is made
 */
- (void)respondWithFIDOForChallenge:(ONGFIDOChallenge *)challenge;

/**
 * The method providing pin the fallback response to the SDK.
 *
 * @param challenge FIDO challenge for which the response is made
 */
- (void)respondWithPinFallbackForChallenge:(ONGFIDOChallenge *)challenge;

/**
 * The method to cancel the challenge
 *
 * @param challenge FIDO challenge that needs to be cancelled
 */
- (void)cancelChallenge:(ONGFIDOChallenge *)challenge;

@end

/**
 * Represents authentication with FIDO challenge. It provides all information about the challenge and a sender awaiting for a response.
 */
@interface ONGFIDOChallenge : NSObject

/**
 * The user profile for which authenticate with FIDO challenge was sent
 */
@property (nonatomic, readonly) ONGUserProfile *userProfile;

/**
 * The FIDO Authenticator that is being used for this challenge
 */
@property (nonatomic, readonly) ONGAuthenticator *authenticator;

/**
 * The error describing the cause of failure for a previous challenge response.
 * The domain of the error: ONGGenericErrorDomain
 *
 * IMPORTANT: Currently not in use.
 */
@property (nonatomic, readonly, nullable) NSError *error;

/**
 * The sender awaiting for a response to the authentication with FIDO challenge.
 */
@property (nonatomic, readonly) id<ONGFIDOChallengeSender> sender;

@end

NS_ASSUME_NONNULL_END