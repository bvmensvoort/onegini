//  Copyright © 2017 Onegini. All rights reserved.

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ONGCustomAuthenticatorInfo : NSObject

@property (nonatomic) NSInteger status;
@property (nonatomic,copy) NSString *data;

@end

NS_ASSUME_NONNULL_END